#!/usr/bin/python
# coding:utf-8
import numpy as np
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data

tf.logging.set_verbosity(tf.logging.INFO)  # 将日志输出到屏幕
mnist = input_data.read_data_sets("")  # todo 输入地址

feature_columns = [tf.feature_column.numeric_column("image", shape=[784])]

estimator = tf.estimator.DNNClassifier(  # 定义神经网络模型
    feature_columns=feature_columns,  # 指定需要的数据
    hidden_units=[5000],  # 给出了神经网络的结构(DNN只能定义全连接层,此处给出了隐藏节点数)
    n_classes=10,  # 指定输出的总类目数量
    optimizer=tf.train.AdamOptimizer(),  # 指定优化函数
    model_dir=""  # 模型训练数据保存位置
)

train_input_fn = tf.estimator.inputs.numpy_input_fn(  # 定义数据输入
    x={"image":mnist.train.images},
    y=mnist.train.labels.astype(np.int32),
    num_epochs=None,
    batch_size=128,
    shuffle=True
)

estimator.train(input_fn=train_input_fn, steps=10000)  # 训练模型

test_input_fn = tf.estimator.inputs.numpy_input_fn(
    x={"image":mnist.test.images},
    y=mnist.test.labels.astype(np.int32),
    num_epochs=1,
    batch_size=128,
    shuffle=False
)

accuracy_score = estimator.evaluate(input_fn=test_input_fn)["accuracy"]
print("\nTest accuracy:", accuracy_score*100)
