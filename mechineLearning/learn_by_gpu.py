#!/usr/bin/python
# coding:utf-8
import tensorflow as tf
"""
1. 可以使用tf.device("/gpu:0")将运算指定到特定的设备上
2. log_device_placement = True 可以将计算的每一个设备打印出来
3. 在配置号gpu的环境中, tf会优先选择gpu. 但是不同版本的tf对gpu的支持不一样
4. 通过指定allow_soft_placement = True, 如果运算无法在cpu上运行, 则会自动放到cpu上执行
"""

a_cpu = tf.Variable(0,name="a_cpu")
with tf.device("/gpu:0"):
    a_gpu = tf.Variable(0,name="a_gpu")

sess = tf.Session(
    config=tf.ConfigProto(
        allow_soft_placement = True,
        log_device_placement = True
    )
)

sess.run(tf.initialize_all_variables())
